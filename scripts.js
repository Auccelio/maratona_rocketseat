const Modal = {
    open(){
        // Open modal -->  
        // Add Class Active to Modal -->
        document.querySelector('.modal-overlay') //return an object modal-overlay
        .classList.add('active'); //add active to the object modal-overlay with the pre-function DOM .classList
    },

    close(){
        // Close modal -->
        // Remove Class Active from Modal -->
        document.querySelector('.modal-overlay') //return the object modal-overlay
        .classList.remove('active'); //remove active from the object modal-overlay with pre-function DOM .classList
    }
}


//Here We're gonna use the Web Local Storage to save our datas taken from the users. But we can do this only with Strings.
//That's why We'll transform our Array in Strings in the SET Function and then transform them back to Array in the GET Function 
const Storage = {
    get(){
        return JSON.parse(localStorage.getItem("dev.finances:transactions")) || [] //here there is a transformation from Strings to Array back again or return an empty Array
    },

    set(transactions){
        localStorage.setItem("dev.finances:transactions", JSON.stringify(transactions)) //here there is a transformation from Array to Strings
    }
}

const Transactionn = {
    all: Storage.get(),

    add(transaction){
        Transactionn.all.push(transaction)

        App.reload()
    },

    remove(index){
        Transactionn.all.splice(index, 1) //Splice function is used with arrays to remove some index for example

        App.reload()
    },

    Incomees(){
        let income = 0;
        //For each transaction we'll have the sum of every value/amount greater than 0
        Transactionn.all.forEach(Transactionss => {
            if (Transactionss.amount > 0){
                income += Transactionss.amount
            }
        })
        
        return income;
    },

    Expensees(){
        let expense = 0;
        //For each transaction we'll have the sum of every value/amount less than 0
        Transactionn.all.forEach(Transactionss => {
            if (Transactionss.amount < 0){
                expense += Transactionss.amount
            }
        })
        return expense;
    },

    Total(){
        let total = 0;
        //For each transaction we'll have the balance from every incomes and expenses value/amount
        total = Transactionn.Incomees() + Transactionn.Expensees(); 
        return total
    }
}

const DOM = {
    transactionContainer: document.querySelector('#tableaux tbody'),

    addTransaction(Transactionss, index){
        //console.log(Transactionss)
        const tr = document.createElement('tr') //create only tr
        tr.innerHTML = DOM.innerHTMLTransaction(Transactionss, index) //put inside tr all the content from td
        tr.dataset.index = index

        //console.log(tr.innerHTML)
        DOM.transactionContainer.appendChild(tr)
    },

    innerHTMLTransaction(Transactionss, index){
        const CSSclass = Transactionss.amount > 0 ? "incomee" : "expensee";

        const amount = Utils.formatCurrency(Transactionss.amount)

        const html = `
            <td class="descriptionn">${Transactionss.description}</td>
            <td class="${CSSclass}">${amount}</td>
            <td class="datee">${Transactionss.date}</td>
            <td>
                <img onclick="Transactionn.remove(${index})" src="assets/assets/minus.svg" alt="Removing Transaction">
            </td>
            `
            return html
    },

    updateBalance(){
        document.getElementById('incomeDisplay')
        .innerHTML = Utils.formatCurrency(Transactionn.Incomees())

        document.getElementById('expenseDisplay')
        .innerHTML = Utils.formatCurrency(Transactionn.Expensees())

        document.getElementById('totalDisplay')
        .innerHTML = Utils.formatCurrency(Transactionn.Total())
    },

    clearTransactions(){
        DOM.transactionContainer.innerHTML = " "
    }
}

const Utils = {
    //remember that all the values sent from form are Strings
    formatAmount(value){
        value = Number(value) * 100
                                                //console.log(value)
        return value
    },

    formatDate(date){
        const splittedDate = date.split("-")
                                                //console.log(splittedDate)
        return `${splittedDate[2]}/${splittedDate[1]}/${splittedDate[0]}` //using template literals to format the date
    },


    formatCurrency(value){
        const signal = Number(value) < 0 ? '-' : "+"
        value = String(value).replace(/\D/g, " ") //search in a global search everything which is not a number and replace for nothing
        value = Number(value) / 100
        value = value.toLocaleString("pt-BR", { //Function to format numbers in currency (R$)
            style: "currency",
            currency: "BRL"
        })
        return signal + value;
    }

}

const Form = {
    description: document.querySelector('input#description'),
    amount: document.querySelector('input#amount'),
    date: document.querySelector('input#date'),

    getValues(){
        return {
            description: Form.description.value,
            amount: Form.amount.value,
            date: Form.date.value
        }
    },

    formatValues(){
        //getting information from the user
        let { description, amount, date} = Form.getValues()
        amount = Utils.formatAmount(amount)
                                                //console.log('Formated Data.')
        date = Utils.formatDate(date)
                                                //console.log(date)
        return {
            description,
            amount, 
            date
        }
    },
    
    validateFields(){
        //getting information from the user
        const { description, amount, date} = Form.getValues()

        //Function trim() clean the empty places in Strings
        if(description.trim() === '' || amount.trim() === '' || date.trim() === ''){
            throw new Error("Please, fill all the fields.")
        } 
                                                //console.log(description)
    },

    saveTransaction(transaction){
        Transactionn.add(transaction)
    },

    clearFields(){
        Form.description.value = ""
        Form.amount.value = ""
        Form.date.value = ""
    },
    

    submit(event){
        event.preventDefault()

        try {
            Form.validateFields()
            const transaction = Form.formatValues()
            Form.saveTransaction(transaction)
            Form.clearFields()
            Modal.close()
        } catch (error) {
            alert(error.message)    
        }

    }

}



const App = {
    init (){
        //DOM.addTransaction(Transactionss[0]) //this way is not so good to solve this task

        //This way bellow is much better to solve this task, it'll become dynamic
        Transactionn.all.forEach(DOM.addTransaction) 
        
        DOM.updateBalance()

        Storage.set(Transaction.all)
    },
    reload (){
        DOM.clearTransactions()
        App.init()
    },
}

App.init()

//Transactionn.remove(3)

Storage.get()
Storage.set("Salut.")
